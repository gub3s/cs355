/**
	UPDATE!!! The shell script has been updated so you can either run

		java cs355/solution/CS355
			
			-or-

		java -jar CS355.jar

	or double-click the JAR file in the file system.
**/

/** In order to compile project first run:

	./script.sh

    This will make sure all files ending ‘.java’ are added to the sources.txt file.  After that you can run:

	javac @sources.txt
    
    This will ensure all ‘.java’ files are compiled.  To run the program, run:

	java cs355/solution/CS355

    In order to make a complete executable JAR file, run:

	jar -cvfe CS355.jar cs355/solution/CS355 cs355/

    Then you can either click on the jar in the file system or run:

	java -jar CS355.jar

**/

// This command will recursively go through the current directory and add any file with ending '.java' to sources.txt

find ./ -name "*.java" > sources.txt;

