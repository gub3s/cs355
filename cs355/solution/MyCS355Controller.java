package cs355.solution;

import cs355.CS355Controller;

import java.awt.Color;
import java.awt.image.BufferedImage;
import java.util.Iterator;

public class MyCS355Controller implements CS355Controller {
    
    public void colorButtonHit(Color c)
    {
        System.out.println("Color Button!!!!!!");
    }
    
    public void triangleButtonHit()
    {
        
    }

    public void squareButtonHit()
    {
        
    }

    public void rectangleButtonHit()
    {
        
    }

    public void circleButtonHit()
    {
        
    }

    public void ellipseButtonHit()
    {
        
    }
    
    public void lineButtonHit()
    {
        
    }
    
    public void selectButtonHit()
    {
        
    }
    
    public void zoomInButtonHit()
    {
        
    }
    
    public void zoomOutButtonHit()
    {
        
    }
    
    public void hScrollbarChanged(int value)
    {
        
    }
    
    public void vScrollbarChanged(int value)
    {
        
    }
    
    public void toggle3DModelDisplay()
    {
        
    }
    
    public void keyPressed(Iterator<Integer> iterator)
    {
        
    }
    
    public void doEdgeDetection()
    {
        
    }
    
    public void doSharpen()
    {
        
    }
    
    public void doMedianBlur()
    {
        
    }
    
    public void doUniformBlur()
    {
        
    }
    
    public void doChangeContrast(int contrastAmountNum)
    {
        
    }
    
    public void doChangeBrightness(int brightnessAmountNum)
    {
        
    }
    
    public void doLoadImage(BufferedImage openImage)
    {
        
    }
    
    public void toggleBackgroundDisplay()
    {
        
    }
}